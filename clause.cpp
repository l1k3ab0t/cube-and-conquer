#include <cstddef>
#include <iostream>
#include <vector>
#include <string>
#include <assert.h>
#include <algorithm>

#ifndef _CLAUSEH_
#define _CLAUSEH_
#include "clause.h"
#endif

using namespace std;

literal to_lit(string str) {
    //cout << str << "\n";
    int i = stoi(str);
    return to_lit(abs(i)-1, i>0);
}

literal to_lit(unsigned int var, bool sign) {
    return var<<1 | sign;
}

string as_str(literal lit) {
    string prefix = lit & 1? "": "!";
    string var = to_string((lit>>1)+1);
    return prefix+var;
}

string as_str(vector<literal> lits) {
    string str = "";

    for (auto lit : lits) {
        str+=(as_str(lit)+", ");
    }
    str = str.substr(0, str.length()-2);

    return "("+str+")";
}

bool is_negation(literal lit1, literal lit2) {
    if (lit1 >> 1 != lit2 >> 1) return false;
    return lit1 != lit2;    
}

const literal negate_lit(const literal lit) {
    //return lit & 1? ((lit>>1)<<1): lit|1;
    return lit ^ 1;
}
Clause::Clause() {
    this->learned = false;
    this-> literals = vector<literal>();
    this->activity = 0;
    this->trail = vector<literal>();
    this->trail_steps = vector<unsigned int>();

}

Clause::Clause(vector<literal> literals) {
    assert(literals.size()>0);

    this->learned = false;
    this->literals = literals;
    this->activity = 0;
    this->trail = vector<literal>();
    this->trail_steps = vector<unsigned int>();
  
}
Clause::Clause(vector<literal> literals, bool learned) {
    assert(literals.size()>0);

    this->learned = learned;
    this->literals = literals;
    this->activity = 0;
    if (learned)
    {
        this->activity = 1;
    }
    
    this->trail = vector<literal>();
    this->trail_steps = vector<unsigned int>();
}



Clause::Clause(string row) {
    assert(row.length()>0);

    int i = 0;

    vector<literal> literals;

    do {
        int end = row.find(" ", i);
        literals.push_back(to_lit(row.substr(i, end)));

        if (end == string::npos) break;
        i = end+1;
    }while(i<row.length());

    ////literals.push_back(to_lit(row.substr(i, row.length()-1)));

    this->literals = literals;
    this->activity = 0;
    this->trail = vector<literal>();
    this->trail_steps = vector<unsigned int>();



}

Clause::Clause(Clause *old) {
    Clause(vector<literal>(old->literals), old->learned);
}

Clause::Clause(literal lit) {
    this->literals = {lit};
    this->activity = 0;
    this->trail = vector<literal>();
    this->trail_steps = vector<unsigned int>();
}

Clause::Clause(unsigned int var, bool prefix) {
    Clause(var<<1|!prefix);
}

string Clause::str() {
    return as_str(this->literals);
}

bool Clause::vertify(vector<bool> &model) {
    for (literal lit: this->literals) {
        if (model[var(lit)] == sign(lit)) 
            return true;
    }
    return false;
}

// 0=var not clause, 1=clause fullfiled, -1=clause empty
int Clause::propagate(literal lit, unsigned int dept) {
    literal negation = negate_lit(lit);
    for (int i=0; i<this->length(); i++) {
        auto lit2 = this->literals.at(i);
        if (lit == lit2) return 1;
        if (is_negation(lit, lit2)) {
            while (dept > this->trail_steps.size()) {
                this->trail_steps.push_back(0);
            }
            this->trail_steps[dept-1]++;
            this->trail.push_back(lit2);
            this->literals.erase(this->literals.begin()+i);
            return this->literals.empty()? -1 : 0;
        }
    }
    return 0;
}

int Clause::propagate(vector<literal> &lits, unsigned int dept) {
    for (auto lit:lits) {
        int prop = this->propagate(lit, dept);
        if (prop != 0) return prop;
    }
    return 0; 
}

int Clause::propagate(set<literal> &lits, unsigned int dept) {
    for (auto lit:lits) {
        int prop = this->propagate(lit, dept);
        if (prop != 0) return prop;
    }
    return 0; 
}

void Clause::undo_prop(unsigned int dept) {
    if (this->trail_steps.size() <= dept) return;

    int undo_c = 0;
    while (this->trail_steps.size() > dept)
    {
        undo_c += this->trail_steps.back();
        this->trail_steps.pop_back();
    }
    this->literals.insert(
        this->literals.end(), 
        std::make_move_iterator(this->trail.end()-undo_c), 
        std::make_move_iterator(this->trail.end()));
    this->trail.erase(this->trail.end()-undo_c, this->trail.end());
}

set<unsigned int> Clause::vars() {
    set<unsigned int> vars;

    for (auto lit:this->literals) {
        vars.insert(lit>>1);
    }
    return vars;
}

set<literal> Clause::lits() {
    return set<literal> (this->literals.begin(), this->literals.end());
}

vector<literal> *Clause::lits_ptr() {
    return &this->literals;
}

set<literal> Clause::pure(set<literal>&lits) {
    for(auto lit:this->literals) {
        auto negated = negate_lit(lit);
        if (lits.contains(negated)) {
            lits.erase(negated);
        }else {
            lits.insert(lit);
        }
    }
    return lits;
}


vector<literal> Clause::reason(optional<literal> lit) {
    vector<literal> reason;
    if (learned) {
        this->activity += 1;
    }
    for(int i = lit? 1:0; i < this->length(); i++) {
        reason.push_back(negate_lit(this->literals.at(i)));
    }
    return reason;
}


void Clause::shrink(unsigned int size) {
    this->literals.resize(size);
}

void Clause::undo(literal p) {
    //TODO Implement
}


set<unsigned int> vars(vector<Clause> &clauses) {
    set<unsigned int> vars;
    for (auto c: clauses) {
        auto cvars = c.vars();
        vars.insert(cvars.begin(), cvars.end());
    }
    
    return vars;
}

bool operator==(const Clause &lc, const Clause &rc) {
    return (lc.literals == rc.literals) && lc.learned == rc.learned;
}