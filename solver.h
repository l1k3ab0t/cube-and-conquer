#include <vector>
#include <queue>
#include <string>
#include <tuple>
#include <optional>
#include <memory>


#ifndef _CLAUSEH_
#define _CLAUSEH_
#include "clause.h"
#endif

using namespace std;
using Clause_ptr = shared_ptr<Clause>;
using Clause_ref = weak_ptr<Clause>;

enum Result {Sat, Unsat, Unknown};


static const unsigned int ROOT_LEVEL = 0;

class Solver {
    public:
        virtual bool solve(vector<literal> assumps) = 0;

};