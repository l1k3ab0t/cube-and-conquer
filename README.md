## Another random SAT Solver
based on https://www.cs.utexas.edu/~marijn/publications/cube.pdf


### Building
```
mkdir build
cd build
cmake ..
make all
```

### Usage

`./cube /path/to/*.cnf`
