#include <cstddef>
#include <cstdlib>
#include <optional>
#include <vector>
#include <string>
#include <set>
#include <algorithm>
#include <cassert>
#include <math.h>
#include <iomanip>

#include "lookahead.h"
#include "cdcl.h"
#include "../util.h"

using namespace std;

void LookAheadSolver::unassign(set<literal> *lits) {
    for (auto lit: *lits) {
        this->assignments[var(lit)] = Unassigned;
    }
}

int LookAheadSolver::propergate(literal lit) {
    vector<literal> lits;
    lits.push_back(lit);
    return propergate(lits);
}
int LookAheadSolver::propergate(vector<literal> lits) {
    dept++;
    this->trail_steps.push_back(this->trail.size());
    this->assign_trail_steps.push_back(this->assign_trail.size());
    set<literal> assigned;
    queue<literal> prop_queue;
    for(auto lit: lits)
    {
        assigned.insert(lit);
        prop_queue.push(lit);
    }
    literal lit;
    
    while (!prop_queue.empty())
    {

        lit = prop_queue.front();
        this->assign_trail.push_back(lit);

        this->assignments[var(lit)] = sign(lit)? True: False;
        for (auto clause: literal_lut[negate_lit(lit)]) {
            auto c_ptr = clause.lock();
            if(this->constrains.contains(c_ptr)) {
                int prop_res = c_ptr->propagate(lit, dept);
                if (prop_res == -1) {
                    this->unassign(&assigned);
                    return -1;
                } else if(prop_res == 1) {
                    this->constrains.erase(c_ptr);
                    this->trail.push_back(c_ptr);
                } else if (c_ptr->length() == 1) {
                    literal next = c_ptr->at(0);
                    if (assigned.contains(negate_lit(next))) {
                        this->unassign(&assigned);
                        return -1;
                    }
                    assert(assignments[var(next)] == Unassigned);
                    if (!assigned.contains(next)) {
                        assigned.insert(next);
                        prop_queue.push(next);
                    }
                    this->constrains.erase(c_ptr);
                    this->trail.push_back(c_ptr);
                }
            }
        }
        for (auto clause: literal_lut[lit]) {
            auto c_ptr = clause.lock();
            if(this->constrains.contains(c_ptr)) {
                this->constrains.erase(c_ptr);
                this->trail.push_back(c_ptr);
            }

        }
        prop_queue.pop();
    }
    return assigned.size()-lits.size();
}

void LookAheadSolver::undo_last() {
    if (dept == 0) return;
    dept--;
    int back = trail_steps.back();
    int c = trail.size() - back;
    for(; c > 0; c--) {
        auto last = trail.back();
        constrains.insert(last);
        trail.pop_back();
    }
    trail_steps.pop_back();
    back = assign_trail_steps.back();
    c = assign_trail.size() - back;
    for(; c > 0; c--) {
        auto last = assign_trail.back();
        assignments[var(last)] = Unassigned;
        for (auto clause: literal_lut[negate_lit(last)]) {
            clause.lock()->undo_prop(dept);
        }
        assign_trail.pop_back();
    }
    assign_trail_steps.pop_back();
}

double weight(unsigned int size) {
    switch (size){
        case 2:
            return 1.0;
        case 3:
            return 0.2;
        case 4:
            return 0.05;
        case 5:
            return 0.01;
        case 6:
            return 0.003;
        default:
            return 20.4514*pow(0.218673, 1.0*size);    
    }
}

tuple<optional<literal>, vector<literal>> LookAheadSolver::lookahead() {
    double max_w = 0;
    unsigned int max = -1;
    vector<literal> forced;

    //cout << "LookAhead Table:" << endl;

    for (unsigned int var = 0; var < var_cnt; var++)
    {
        if (assignments[var] == Unassigned) {
            literal lit = to_lit(var, true);
            double w = 0.0;
            double w2 = 0.0;
            int res = this->propergate(lit);
            if (res == -1) {
                forced.push_back(negate_lit(lit));
                w = -1;
            }else {
                for (auto clause: constrains) { 
                    if(clause->was_learned(dept)) {
                        w += weight(clause->length());
                    }
                }
                }
            this->undo_last();
            
            res = this->propergate(negate_lit(lit));
            if (res == -1) {
                if(w<0) {
                    this->undo_last();
                    //cout << "Lookahead failed for var:" << as_str(lit) << endl;
                    return {nullopt, {}};
                }
                forced.push_back(lit);
                w2 = -1;
            }else {
                for (auto clause: constrains) {
                    if(clause->was_learned(dept)) {
                        w2 += weight(clause->length());
                    }
                } 
            }
            this->undo_last();
            //cout << as_str(lit) << ":\t " << setprecision(3) << w <<"*"<<w2<< endl;
            if(w*w2>=max_w && w+w2>=0) {
                max_w = w*w2;
                max = lit;
            }
        }
    }
    //cout << "Decission: " << as_str(max) << ", forced literals: " << forced.size() << endl;
    return {max, forced};
}

void LookAheadSolver::add_clause(Clause *clause) {
    Clause_ptr ptr = make_shared<Clause>();
    ptr.reset(new Clause(*clause->lits_ptr()));
    constrains.insert(ptr);
    for(int i=0; i<clause->length(); i++){
        literal_lut[clause->at(i)].push_back(ptr);
    }
}

set<vector<literal>> LookAheadSolver::gen_cubes() {
    dpll(true, 500, 0);
    return cubes;
}

bool LookAheadSolver::dpll() {
    return dpll(false, 1000, 0);
}

bool LookAheadSolver::dpll(bool enable_cutoff, double threshold, unsigned int assigned_vars) {
    if (this->all_assigned() || this->constrains.empty()) return true;
    if(enable_cutoff) {
        //cout << "Threshold="<<threshold<<" dept="<<dept<<" f="<<(1028.0*dept*assigned_vars)<<">?"<<(threshold*var_cnt)<<endl;
        threshold *=1.05;
        if(dept>5) threshold *= 0.7;
        if(1028.0*dept*assigned_vars>threshold*(1.0*var_cnt)) {
            vector<literal> cube;
            for (unsigned int var=0; var < var_cnt; var++) {
                if(assignments[var] == True) {
                    cube.push_back(to_lit(var, true));
                }else if (assignments[var] == False) {
                    cube.push_back(to_lit(var, false));
                }
            }
            cubes.insert(cube);
            //cout << "Adding cube " << as_str(cube) << "cube cout="<< cubes.size()<< endl;
            return false;
        }
    }
    
    optional<literal> opt_lit;
    vector<literal> forced_lits;

    tie(opt_lit, forced_lits) = this->lookahead();
    if(!opt_lit) return false;

    //if(!forced_lits.empty() && this->propergate(forced_lits)<0) {
    //    this->undo_last();
    //    return false;
    //}
    //if(dept > 10 && !forced_lits.size()>0) threshold *= (0.7/forced_lits.size());
    int prop_result;
    forced_lits.push_back(*opt_lit);
    prop_result = this->propergate(forced_lits);
    PRINT_CLAUSES(this->constrains, "Afer Propergation");
    if (prop_result != -1) {
        if(this->dpll(enable_cutoff, threshold, assigned_vars+1+prop_result+forced_lits.size())) return true;
    }
    this->undo_last();
    PRINT_CLAUSES(this->constrains, "After undo:");

    forced_lits.pop_back();
    forced_lits.push_back(negate_lit(*opt_lit));
    prop_result = this->propergate(forced_lits);
    PRINT_CLAUSES(this->constrains, "Afer Propergation of negation");
    if (prop_result != -1) {
        if(this->dpll(enable_cutoff, threshold, assigned_vars+1+prop_result+forced_lits.size())) return true;
    }
    this->undo_last();
    //if(!forced_lits.empty())
    //{
    //    this->undo_last();
    //}
    
    PRINT_CLAUSES(this->constrains, "After undo:");
    return false;
}

bool vertify_model(vector<Clause> &clauses, vector<bool> &model) {
    for (auto c: clauses) {
        if (!c.vertify(model)) {
            //cout << "Failed to vertify, " << c.str() << endl;
            return false;
        }
    }
    return true;
}

bool LookAheadSolver::_solve(vector<literal> assumps) {
    //TODO treat assumps as unit facts
    PRINT_CLAUSES(this->constrains, "Start:");
    bool res = this->dpll();
   
    return res;
}

bool lookahead(vector<Clause> clauses) {
    set<literal> vars;
    for (auto c: clauses) {
        auto cvars = c.vars();
        vars.insert(cvars.begin(), cvars.end());
    }
    auto solver = LookAheadSolver(vars.size());
    for (auto clause: clauses) {
        solver.add_clause(&clause);
    } 
    auto res = solver.solve({});
    auto model = solver.gen_model();
    if (res) {
        cout << "v ";
        for (int i = 0; i < vars.size(); i++)
        {
            cout << (model[i]? "":"-") << i+1 <<" ";
        }
        cout << "0\n";
        assert(vertify_model(clauses, model));
    }
    return res;
}


bool cubeAndConq(vector<Clause> clauses) {
    set<literal> vars;
    for (auto c: clauses) {
        auto cvars = c.vars();
        vars.insert(cvars.begin(), cvars.end());
    }

    auto lah_solver = LookAheadSolver(vars.size());
    for (auto clause: clauses) {
        lah_solver.add_clause(&clause);
    }

    //TODO: maybe run lookahead if less then 50 vars? 
    auto cubes = lah_solver.gen_cubes();

    //TODO: handle clause learning?
    auto cdcl_solver = CDCLSolver(vars.size());
    for (auto clause: clauses) {
        optional<Clause_ptr> temp;
        bool succ;
        tie(succ, temp) = cdcl_solver.add_clause(clause.lits_ptr(), false);
        if(!succ) return false;
    }

    for(vector<literal> cube: cubes) {
        //cout << "Trying to solve cube " << as_str(cube) << endl;
        if (cdcl_solver.solve(cube)) 
        {
            return true;
        }
        cdcl_solver.reset();
        
    }
    return false;
}