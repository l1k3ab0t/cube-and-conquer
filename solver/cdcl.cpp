#include <cstddef>
#include <cstdlib>
#include <optional>
#include <vector>
#include <string>
#include <set>
#include <algorithm>
#include <cassert>

#include "cdcl.h"
#include "../util.h"

using namespace std;

void CDCLSolver::assign(unsigned int var, Assignment assign) {
    assignments[var] = assign;
}

void CDCLSolver::undoOne() {

    literal p = this->trail.back();
    //cout << "Undoing:" << as_str(p) << endl;
    auto x = var(p);
    vars.undo(x);

    this->assignments[x] = Unassigned;
    this->reasons[x] = nullopt;
    this->trail.pop_back();
    this->levels[x] = -1;

    //TODO find usage for this
    //while (this->undos[x].size()>0) {
    //    undos[x].back().undo(x);
    //    undos[x].pop_back();
    //}
}
//TODO make Clause to pointer
tuple<vector<literal>, int> CDCLSolver::analyse(Clause_ptr conflict) {
    //cout << "Found conflic, analysing: " << conflict->str()<< "\n";
    vector<bool> seen (this->var_cnt, false);
    int cnt = 0;
    optional<literal> p = nullopt;
    vector<literal> p_reason;
    vector<literal> learned = {0};
    int btlevel = 0;

    do {
        p_reason = conflict->reason(p);
        if (p) {
            //cout << "Conflict reason: " << as_str(p_reason) << ", for: " << as_str(*p) << "\n";
        }else {
            //cout << "Initial Conflict reason: " << as_str(p_reason) << "\n";
        }

        for (int i=0; i < p_reason.size(); i++) {
            literal l = p_reason.at(i);
            assert(l<1000);
            if(!seen[var(l)]) {
                seen[var(l)] = true;
                int level = this->levels[var(l)];
                if(level == this->decision_level) {
                    cnt++;
                }else if(level > 0) {
                    learned.push_back(negate_lit(l));
                    if (level > btlevel) btlevel = level;
                }
            
            }
        }
        do {
            p = this->trail.back();
            if (reasons[var(*p)]) {
                conflict = (*reasons[var(*p)]);
            } else {
                conflict.reset(new Clause());

            }
            this->undoOne();
        } while(!seen[var(*p)]);
        cnt--;
    
    } while(cnt > 0);
    learned[0] = negate_lit(*p);
    //cout << "Learned clause: " << as_str(learned) << "\n";

    return {learned, btlevel};
}
bool check_assignment(literal lit, optional<Assignment> assignment) {
    if(!assignment || *assignment == Unassigned) return false;
    return (*assignment == True) == sign(lit);
}

bool CDCLSolver::propergate_clause(Clause_ptr clause, literal lit) {
    //cout << "Propergating lit=" << as_str(lit) << ", over " << clause->str()<< endl;
    auto lits = clause->lits_ptr(); //TODO make sure that this works and not a variable is needed
    if (is_negation(lit, lits->at(0)) && lits->size() > 1) {
        lits->at(0) = lits->at(1);
        lits->at(1) = negate_lit(lit);
    }

    if(check_assignment(lits->at(0), assignments[var(lits->at(0))])) {
        watches[lit].push_back(clause);
        return true;
    }

    for (int i=2; i <lits->size(); i++) {
        literal liti = lits->at(i);
        auto assignment = assignments[var(liti)];
        if (!check_assignment(negate_lit(liti), assignment)) {
            lits->at(1) = liti;
            lits->at(i) = negate_lit(lit);
            watches[negate_lit(liti)].push_back(clause);
            return true;
        }
    }
    watches[lit].push_back(clause);
    //cout << "Enqueing due to unit fact" << clause->str() << endl;
    return enqueue(lits->at(0), clause);
}

optional<Clause_ref> CDCLSolver::propagate() {
    while(!this->prop_queue.empty()) {
        literal p = this->prop_queue.front();
        this->prop_queue.pop();
        //TODO move instead of copy
        vector<Clause_ref> tmp = this->watches[p];
        this->watches[p] = {};

        for(int i=0; i < tmp.size(); i++) {
            Clause_ref clause = tmp[i];
            if(!clause.expired() && !propergate_clause(clause.lock(), p)) {
                //std::move(tmp.begin()+i+1, tmp.end(), this->watches.begin());
                for (int j=i+1; j<tmp.size(); j++) {
                    this->watches[p].push_back(tmp[j]);
                }
                return tmp[i];
            } 
        }
    }
    return nullopt;
}

void CDCLSolver::cancel() {
    if (trail_steps.size() == 0) return;
    int back = trail_steps.back();
    int c = trail.size() - back;
    for(; c > 0; c--) {
        undoOne();
    }
    decision_level--;
    trail_steps.pop_back();
}

bool CDCLSolver::enqueue(literal lit, optional<Clause_ref> origin) {
    //TODO make sure that this works
    auto assignment = assignments[var(lit)];
    //cout << "enqueing: "
        //<< as_str(lit) 
        //<< ", current assignment: " 
        //<< (assignment == nullopt || *assignment==Unassigned?"unassigned":"assigned");
    //if (origin)
        //cout << " origin=" << (*origin).str();
    //cout << "\n";
    if (assignment && *assignment != Unassigned) {
        //cout << "Assignment:" << ((*assignment == True)? "True":"False") << " sign:" << sign(lit) << endl;
        if ((*assignment == True) == sign(lit)) {
            return true;
        }
        std::queue<literal> empty;
        std::swap( prop_queue, empty);
        //cout << "clearing queue" << endl;
        return false;
    }
    auto i = var(lit);
    assignments[i] = sign(lit)? True: False;
    levels[i] = decision_level;
    if(origin) {
        reasons[i] = (*origin).lock();
        (*reasons[i])->lock();
    } else {
        reasons[i] = nullopt;
    }
    trail.push_back(lit);
    prop_queue.push(lit);
    return true;
}

bool CDCLSolver::assume(literal lit) {
    trail_steps.push_back(trail.size());
    return enqueue(lit, nullopt);
}

tuple<bool, optional<Clause_ptr>> CDCLSolver::add_clause(vector<literal>  *lits, bool learned) {
   if (lits->empty()) return {false, nullopt};


   if (!learned) {
       set<literal> s (lits->begin(), lits->end());
       if (s.size()!=lits->size()) {
           lits->erase(lits->begin()+s.size(), lits->end());
           lits->insert(lits->begin(), s.begin(), s.end());
       }
   }
   if(lits->size()==1) {
        //TODO maybe return the result in some form?
        auto lit = lits->at(0);
        //cout << "Adding unit clause " << as_str(lit) << "\n";
        return {enqueue(lit, nullopt), nullopt};
   }

   Clause_ptr c = make_shared<Clause>();
   
   if (learned) {
        int max = 0;
        int max_lvl = levels[var(lits->at(0))];

        for(int i=1; i<lits->size(); i++) {
            int level = levels[var(lits->at(i))];
            if (level>max_lvl) {
                max = i;
                max_lvl=level;
            }
        }
        literal temp = lits->at(max); 
        lits->at(max) = lits->at(1);
        lits->at(1) = temp; 
        c.reset(new Clause (*lits, learned));

        for (auto lit: *lits) {
            activiy[var(lit)] += activiy_step;
            vars.update(var(lit));
        }
        vars.reorder();
   }
   else {
       c.reset(new Clause(*lits, learned));
       constrains.push_back(c);
   }
   //cout << "Adding clause" << c->str() << " learned: " << learned << "\n";
   watches[negate_lit(lits->at(0))].push_back(c);
   watches[negate_lit(lits->at(1))].push_back(c);
 
   return {true, c};
}

void CDCLSolver::record(vector<literal> *lits) {
    optional<Clause_ptr> clause;
    bool temp;
    tie(temp, clause) = add_clause(lits, true);
    if(clause) {
        enqueue(lits->at(0), clause);
        if(clause) learned.push_back(*clause);
    }
}

bool CDCLSolver::simplifiy(Clause_ptr c) {
    int j = 0;
    for(int i = 0; i<c->length(); i++) {
        if(check_assignment(c->at(i), assignments[var(c->at(i))])) {
            return true;
        }else if(!assignments[var(c->at(i))] || (*assignments[var(c->at(i))]) == Unassigned) { //TODO make sure assignments==value
            (c->lits_ptr())->at(j++) = c->at(i);
        }
    }
    c->shrink(c->length()-j);
    return false; 
}

bool CDCLSolver::is_locked(Clause_ptr c) {
    auto reason = reasons[var(c->at(0))];
    return reason && (*reason) == c;
}
void CDCLSolver::unwatch(Clause_ptr c, literal lit) {
    for (int i = 0; i < watches[lit].size(); i++) {
        if (watches[lit][i].lock() == c) { //Make sure that this works
            watches[lit].erase(watches[lit].begin()+i);
            return;
        }
    }
}

void CDCLSolver::remove(Clause_ptr c) {
    auto first = negate_lit(c->at(0));
    auto secound = negate_lit(c->at(1));

    unwatch(c, first);
    unwatch(c, secound);

}

bool CDCLSolver::simplifiyDB() {
    if(propagate()) return false;

    for (int type = 0; type < 2; type++) {
        vector<Clause_ptr> * cs = type? &learned: &constrains;
        int j = 0;
        for (int i=0; i<cs->size(); i++) {
            if (simplifiy(cs->at(i)))
                remove(cs->at(i));
            else cs->at(j++)=cs->at(i);
        }
        cs->erase(cs->end()-j, cs->end());

    }
    return true;
}

void CDCLSolver::reduceDB() {
    if (learned.size() == 0) return;
    int i,j;
    double lim = activiy_step / learned.size();
    sort(learned.begin(), learned.end(), [](Clause_ptr& a, Clause_ptr& b) -> bool
            {
                return a->get_activity() > b->get_activity();
            }); 
    for(i=j=0; i< learned.size()/2; i++) {
        if (!is_locked(learned[i])) {
            remove(learned[i]);
        } else {
            learned[j++] = learned[i];
        }
    }
    if (i!=j)
        learned.erase(learned.end()-(i-j), learned.end());
}

literal CDCLSolver::choose_literal() {
    unsigned int var = vars.get_var();
    while (assignments[var] && assignments[var] != Unassigned) {
        var=vars.get_var();
    }
    return to_lit(var, rand()%2);
}

int CDCLSolver::assigned_vars() {
    int c = 0;
    for (int i=0; i<assignments.size(); i++) {
        if (assignments[i]) c++; 
    }
    return c;
}

void CDCLSolver::reset() {
    vars.reset();
    for (int i=0; i<var_cnt; i++) {
        assignments[i] = Unassigned;
    }
}

Result CDCLSolver::search(int conflicts, int learnts, unsigned int root_level, tuple<double, double>) {
    int conflict_cnt = 0;

    model.clear();

    while(true) {
        auto opt_conflict = propagate();
        if (opt_conflict) {
            conflict_cnt++;

            if(this->decision_level == ROOT_LEVEL) {
                return Unsat;
            }
            vector<literal> learned;
            int backtrack;
            tie(learned, backtrack) = analyse((*opt_conflict).lock());
            unsigned int max = ROOT_LEVEL>backtrack? ROOT_LEVEL:backtrack;
            cancel_until(max);
            record(&learned);
            //TODO decay Activities
            for(auto learned_c: this->learned) {
                learned_c->decay_acitivity();
            }
            this->vars.decay_all();
        }else {
            if (decision_level == ROOT_LEVEL)
                //simplifiyDB();
            if (learned.size() >= learnts + assigned_vars())
                reduceDB();

            //if (var_cnt
            //TODO find way to check that all vars are assigned
            
            if (this->all_assigned()) {
                for (int i = 0; i < var_cnt; i++)
                {
                    model[i] = assignments[i] == True;
                }
                return Sat;
            }

            if (conflict_cnt >= conflicts) {
                cancel_until(root_level);
                return Unknown;
            }


            decision_level++;
            //cout << "decision level=" << decision_level << "\n";
            literal lit = choose_literal();
            assume(lit);
        }
    }
}

bool vertify_model(vector<Clause_ptr> &clauses, vector<bool> &model) {
    for (auto c: clauses) {
        if (!c->vertify(model)) {
            //cout << "Failed to vertify, " << c->str() << endl;
            return false;
        }
    }
    return true;
}

bool CDCLSolver::_solve(vector<literal> assumps) {
    double param0 = 0.95;
    double param1 = 0.999;

    double conflicts = 100;
    double learnts = ((double)constrains.size())/3;
    Result status = Unknown;

    //TODO clean up mess generated by prev assumptions
    for(literal lit: assumps) {
        if (!assume(lit) || propagate().has_value()) {
            cancel_until(0);
            return false;
        }
    }
    int root_level = ROOT_LEVEL;
    while (status == Unknown) {
        //cout << "starting search, " << conflicts << ", " << learnts << endl;
        status = search(conflicts, learnts, root_level, {param0, param1});
        conflicts *= 1.5;
        learnts *= 1.1;
    }
    cancel_until(0);
    if(status == Sat) {
        cout << "v ";
        for (int i = 0; i < var_cnt; i++)
        {
            cout << (model[i]? "":"-") << i+1 <<" ";
        }
        cout << "0\n";
        //assert(vertify_model(constrains, model));
    }
    return status == Sat;
}


bool cdcl(vector<Clause> clauses) {
    set<literal> vars;
    for (auto c: clauses) {
        auto cvars = c.vars();
        vars.insert(cvars.begin(), cvars.end());
    }
    CDCLSolver solver(vars.size());
    for (auto clause: clauses) {
        optional<Clause_ptr> temp;
        bool succ;
        tie(succ, temp) = solver.add_clause(clause.lits_ptr(), false);
        if(!succ) return false;
    }

    return solver.solve({});
}