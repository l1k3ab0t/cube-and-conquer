#include <vector>
#include <queue>
#include <string>
#include <tuple>
#include <optional>
#include <memory>

#include "../heuristic.h"

#ifndef _CLAUSEH_
#define _CLAUSEH_
#include "../clause.h"
#endif
#ifndef _SOLVERH_
#define _SOLVERH_
#include "../solver.h"
#endif

class CDCLSolver: public Solver {
    public:
        CDCLSolver(unsigned int var_c):
            vars(var_c, 0.1),
            activiy(var_c, 0),
            assignments(var_c, nullopt),
            prop_queue(),
            levels(var_c, -1),
            reasons(var_c, nullopt),
            watches(var_c * 2, vector<Clause_ref>()),
            model(var_c, false)
            {
                this->constrains = {};
                
                this->learned = {};
                this->activiy_step = 0.1;
                this->activiy_decay = 0.1;
                this->undos = {};
                this->trail = {};
                this->trail_steps = {};
                this->root_level = ROOT_LEVEL;
                this->var_cnt = var_c;
                this->decision_level = ROOT_LEVEL;

            };

        void unwatch(Clause_ptr c, literal lit);
        void remove(Clause_ptr c);
        bool propergate_clause(Clause_ptr clause, literal lit);
        optional<Clause_ref> propagate();
        bool simplifiy(Clause_ptr c);
        bool is_locked(Clause_ptr c);
        void assign(unsigned int var, Assignment assign);
        bool simplifiyDB();
        void reduceDB();
        void undo(literal lit);
        void undoOne();
        void cancel();
        void reset();
        inline void cancel_until(int level) {
            while (decision_level > level) cancel();
        };

        inline bool all_assigned() {
            for(auto assignment: assignments) {
                if(!assignment || *assignment == Unassigned) {
                    return false;
                }
            }
            return true;
        };
        bool enqueue(literal lit, optional<Clause_ref> origin);
        bool assume(literal lit);
        tuple<bool, optional<Clause_ptr>> add_clause(vector<literal> *lits, bool learned);
        void record(vector<literal> *lits);
        literal choose_literal();
        int assigned_vars();

        tuple<vector<literal>, int> analyse(Clause_ptr c);
        Result search(int conflicts, int learnts, unsigned int root_level, tuple<double, double> params);

        bool _solve(vector<literal>assumps);
        bool solve(vector<literal>assumps) {
            return this->_solve(assumps);
        }
    private:
        vector<bool> model;

        vector<Clause_ptr> constrains;
        vector<Clause_ptr> learned;
        double activiy_step;
        double activiy_decay;

        vector<double> activiy;
        vector<vector<Clause_ref>> watches;
        vector<vector<Clause_ref>> undos;
        queue<literal> prop_queue;

        vector<optional<Assignment>> assignments;
        vector<literal> trail;
        vector<int> trail_steps;
        vector<optional<Clause_ptr>> reasons; //TODO make sure that this shouldt be a weak_ptr
        vector<int> levels;
        int root_level;

        int var_cnt;
        unsigned int decision_level;
        VarGen vars;
};

bool cdcl(vector<Clause> clauses);