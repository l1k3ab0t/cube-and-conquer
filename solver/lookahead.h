#ifndef _SOLVERH_
#define _SOLVERH_
#include "../solver.h"
#endif

class LookAheadSolver: public Solver {
    public:
        LookAheadSolver(unsigned int var_c):
            model(var_c, false),
            assignments(var_c, Unassigned),
            literal_lut(var_c * 2, vector<Clause_ref>()),
            binary_implications(var_c * 2, vector<literal>())
            {
                this->dept = 0;
                this->var_cnt = var_c;
                this->constrains;
                this->cubes;
                this->cutoff_threshold = 1000;
            };
        
        void add_clause(Clause *clause);
        tuple<optional<literal>, vector<literal>> lookahead();
        int propergate(literal lit); 
        int propergate(vector<literal> lits); 
        void undo_last();


        set<vector<literal>> gen_cubes();

        bool dpll(bool enable_cutoff, double threshold, unsigned int assigned_vars);
        bool dpll();
        bool _solve(vector<literal>assumps);
        bool solve(vector<literal>assumps) {
            return this->_solve(assumps);
        }
        void unassign(set<literal> *lits);
        inline bool all_assigned() {
            for(auto assignment: this->assignments) {
                if (assignment == Unassigned) return false;
            }
            return true;
        }

        inline vector<bool> gen_model(){
            for (unsigned int i = 0; i < this->var_cnt; i++)
            {
                if(this->assignments[i] == True) {
                    this->model[i] = true;
                }  
            }
            return this->model;

        }

    private:
        vector<bool> model;

        set<Clause_ptr> constrains;
        vector<Assignment> assignments;
        vector<vector<Clause_ref>> literal_lut;
        vector<vector<literal>> binary_implications;
        vector<Clause_ptr> learned;
        unsigned int dept;
        unsigned int var_cnt;
        vector<int> trail_steps;
        vector<Clause_ptr> trail;
        vector<int> assign_trail_steps;
        vector<literal> assign_trail;
        set<vector<literal>> cubes;
        double cutoff_threshold;
};


bool lookahead(vector<Clause> clauses, vector<literal> cube);

bool lookahead(vector<Clause> clauses);
bool cubeAndConq(vector<Clause> clauses);