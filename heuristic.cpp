#include "heuristic.h"
#include <cstdlib>
#include <vector>
#include <time.h>


bool Compare::operator()(Elem e0, Elem e1) {
    if (e0.second != e1.second) {
        return e0.second > e1.second;
    }
    return rand()%2 == 0;
}


VarGen::VarGen(unsigned int var_cnt, double activity_step) {
    srand(time(NULL));
    this->activity_step = activity_step;
    this->activity = vector(var_cnt, 0.0);
    this->stored = vector(var_cnt, true);
    for (int i = 0; i < var_cnt; i++)
    {
        this->vars.push({i, 0});
    }
}

VarGen::VarGen(unsigned int var_cnt, double activity_step, vector<double> activity) {
    srand(time(NULL));
    this->activity_step = activity_step;
    this->activity = activity;
    this->stored = vector(var_cnt, true);
    for (int i = 0; i < var_cnt; i++)
    {
        this->vars.push({i, activity[i]});
    }
}

void VarGen::update() {
    for (int i=0; i<activity.size(); i++) {
        update(i);
    }
    this->reorder();
}
void VarGen::update(unsigned int var) {
    activity[var]+=activity_step;
}

void VarGen::undo(unsigned int var) {
    if (!stored[var])
        vars.push({var, activity[var]});
    stored[var] = true;
}

void VarGen::decay_all(){
    for (size_t i = 0; i < activity.size(); i++)
    {
        activity[i] *=0.95;
    }
    this->reorder();
}

void VarGen::reorder() {
    priority_queue<Elem, vector<Elem>, Compare> reorderd; 
    for (size_t i = 0; i < activity.size(); i++)
    {
        if (stored[i])
        {
            reorderd.push({i, activity[i]});
        }
    }
    this->vars.swap(reorderd);
}

void VarGen::reset() {
    for (size_t i = 0; i < activity.size(); i++)
    {
        if (!stored[i])
        {
            vars.push({i, activity[i]});
        }
    }
}

unsigned int VarGen::get_var() {
    unsigned int var = vars.top().first;
    stored[var] = false;
    vars.pop();
    update(var);
    return var;
}
