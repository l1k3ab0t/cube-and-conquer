#include "vector"
#include "string"
#include <vector>
#include <iostream>
#include <optional>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include "solver/cdcl.h"
#include "solver/lookahead.h"
#include "util.h"

vector<Clause> gen_test(vector<vector<int>> test_input) {
    vector<Clause> clauses;
    int i = 0;
    for(auto c : test_input){
        vector<literal> lits;
        for (int lit: c) {

            lits.push_back(to_lit(abs(lit)-1, lit>0));
        }
        clauses.push_back(Clause(lits));
    }

    return clauses;
}

bool solve(vector<Clause> clauses) {
    return lookahead(clauses);
}


TEST_CASE("unsolvable_simple") {
    auto c1 = gen_test({{1}, {-1}});
    CHECK_FALSE(solve(c1));

    auto c2 = gen_test(
            {
                {1, 2, 3}, 
                {1, 2, -3}, 
                {1, -2, 3}, 
                {1, -2, -3}, 
                {-1, 2, 3},
                {-1, 2, -3}, 
                {-1, -2, 3}, 
                {-1, -2, -3}});
    CHECK_FALSE(solve(c2));
}


TEST_CASE("solvable_simple") {
    auto c1 = gen_test({{1}});
    CHECK(solve(c1));

    auto c2 = gen_test({{1, 2}, {1, -3}, {-2, 3}});
    CHECK(solve(c2));
}

TEST_CASE("solvable_complex") {
    auto c1 = gen_test({{-1, -3, -4}, {2, 3, -4}, {1, -2, 4}, {1, 3, 4}, {-1, 2, -3}});
    CHECK(solve(c1));
}

TEST_CASE("pure") {
    auto c1 = gen_test({{1, 2}, {1, -3, 5}, {1, -3, 4}});
    CHECK(solve(c1));

    auto c2 = gen_test({{1, 2}, {1, -3}, {-2, 3}});
    CHECK(solve(c2));
}