#include <vector>
#include <set>
#include <array>
#include <string>
#include <optional>

using namespace std;
using literal = unsigned int;
using bit_vec = unsigned int;

inline int var(literal lit) {
    return lit>>1;
}

inline bool sign(literal lit) {
    return lit & 1;
}

literal to_lit(string str);
literal to_lit(unsigned int var, bool sign);
string as_str(literal lit);
string as_str(vector<literal> lits);
bool is_negation(literal lit1, literal lit2);
const literal negate_lit(const literal lit);

enum Assignment {False = 10, True = 11, Unassigned = 12};

class Clause {
    public:
        Clause();
        Clause(vector<literal> literals);
        Clause(vector<literal> literals, bool learned);
        Clause(string row);
        Clause(Clause *old);
        Clause(literal lit);
        Clause(unsigned int var, bool prefix);
        string str();
        int propagate(literal lit, unsigned int dept);
        int propagate(set<literal> &lits, unsigned int dept);
        int propagate(vector<literal> &lits, unsigned int dept);
        void undo_prop(unsigned int dept);
        
        bool vertify(vector<bool> &model);
        set<unsigned int> vars();
        set<literal> lits();
        vector<literal> *lits_ptr();
        set<literal> pure(set<literal>&lits);


        vector<literal> reason(optional<literal> lit);
        void shrink(unsigned int size); //TODO implement
        void undo(literal p);

        bool inline was_learned() {
            return this->learned;
        }
        bool inline was_learned(int dept) {
            return trail_steps.size() == dept && trail_steps.back() > 0;
        }

        bool inline contains_var(unsigned int var) {
             for (auto lit: this->literals){
                if (var == lit>>1) return true;
            }
            return false;
 
        };
        bool inline contains_lit(literal lit) {
            for (auto olit: this->literals){
                if (olit == lit) return true;
            }
            return false;
        };
        bool inline contains_any_lit(set<literal> &lits) {
            for(auto lit:lits) {
                if (this->contains_lit(lit)) return true;
            }
            return false;
        };
        unsigned int inline length() {
            return this->literals.size(); 
        };
        bool inline is_unit_clause() {
            return this->length() == 1;
        };
        bool inline is_empty() {
            return this->length() == 0;
        }
        literal inline peak() {
            return this->literals.front();
        }

        literal inline at(int i) {
            return literals[i];
        }
        const double inline get_activity() {
            return activity;
        }

        inline void decay_acitivity() {
            activity *= 0.99;
        }

        bool inline is_locked() {
            return locked;
        };
        void inline lock() {
            locked = true;
        };
        void inline unlock() {
            locked = false;
        };
        friend bool operator==(const Clause &lc, const Clause &rc);

    private:
        bool learned;
        bool locked;
        double activity;
        vector<literal> literals;
        vector<literal> trail; //TODO: to reduce size keep trail in literals and reorder in place, while trail_steps point to arr index
        vector<unsigned int> trail_steps;

};

set<unsigned int> vars(vector<Clause> &clauses);

