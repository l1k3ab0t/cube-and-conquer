#include <ranges>
#include <algorithm>
#include <iostream>

#ifndef _CLAUSEH_
#define _CLAUSEH_
#include "clause.h"
#endif



void inline PRINT_LITERALS(vector<literal> &lits) {
    #ifdef TESTING
    for (auto l:lits) {
       cout << as_str(l) << ", "; 
    }
    cout << "\n";
    #endif
}

void inline PRINT_LITERALS(set<literal> &lits) {
    #ifdef TESTING
    for (auto l:lits) {
       cout << as_str(l) << ", "; 
    }
    cout << "\n";
    #endif
}


void inline PRINT_CLAUSE(Clause &c) {
    #ifdef TESTING
    cout << c.str();
    #endif
}

void inline PRINT_CLAUSE(shared_ptr<Clause> c) {
    #ifdef TESTING
    cout << c->str();
    #endif
}

void inline PRINT_CLAUSES(vector<Clause> &clauses) {
    #ifdef TESTING
    for (auto c:clauses) {
        PRINT_CLAUSE(c);
        cout << ", ";
    
    }
    cout << "\n";
    #endif
}

void inline PRINT_CLAUSES(set<Clause> &clauses) {
    #ifdef TESTING
    for (auto c:clauses) {
        PRINT_CLAUSE(c);
        cout << ", ";
    
    }
    cout << "\n";
    #endif
}

void inline PRINT_CLAUSES(set<shared_ptr<Clause>> &clauses) {
    #ifdef TESTING
    for (auto c:clauses) {
        PRINT_CLAUSE(c);
        cout << ", ";
    
    }
    cout << "\n";
    #endif
}

void inline PRINT_CLAUSES(vector<Clause> &clauses, std::string str) {
    #ifdef TESTING
    cout << str;
    PRINT_CLAUSES(clauses);
    #endif
}

void inline PRINT_CLAUSES(set<Clause> &clauses, std::string str) {
    #ifdef TESTING
    cout << str;
    PRINT_CLAUSES(clauses);
    #endif
}

void inline PRINT_CLAUSES(set<shared_ptr<Clause>> &clauses, std::string str) {
    #ifdef TESTING
    cout << str;
    PRINT_CLAUSES(clauses);
    #endif
}


template <typename TCont, typename Pred>
auto inline filter(const TCont& vec, Pred p) {
    TCont out;
    copy_if(vec, back_inserter(out), p);
    return out;
}

static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}
// trim from both ends (in place)
static inline void trim(std::string &s) {
    rtrim(s);
    ltrim(s);
}
