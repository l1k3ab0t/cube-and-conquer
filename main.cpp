#include <assert.h>
#include <iostream>
#include <vector>
#include <fstream>
#include <string.h>

#include "solver/cdcl.h"
#include "solver/lookahead.h"
#include "util.h"

using namespace std;

vector<Clause> read_DIMACS(string path) {
    ifstream file(path);
    std::string str; 
    vector<Clause> lits;
    while(getline(file, str)) {
        if (str[0] == 'c') {


        }else if(str[0] == 'p') {
        }else if(str[0] == '%') {
            return lits;
        }else {
            if (str[0] == ' ') {
                //str = str.substr(1, str.length());
                ltrim(str);
            }
            lits.push_back(Clause(str.substr(0, str.length()-2)));
        }
    }
    return lits;    
}

vector<Clause> gen_test(vector<vector<int>> test_input) {
    vector<Clause> clauses;
    int i = 0;
    for(auto c : test_input){
        vector<literal> lits;
        for (int lit: c) {

            lits.push_back(to_lit(abs(lit)-1, lit>0));
        }
        clauses.push_back(Clause(lits));
    }

    return clauses;
}


void test() {
    auto c1 = gen_test({{1}});
    //cout << solve(c1) << "\n";

    auto c2 = gen_test({{1, 2}});
    cubeAndConq(c2);
}

int main(int argc, char* argv[]) {
    assert(argc > 1);

    //cout << argv[1] << "\n";
    if (strcmp(argv[1], "test") == 0) {
        //cout << "Runinng tests...\n";
        test();
    }
     
    auto clauses = read_DIMACS(argv[1]);
    for (auto c : clauses) {
        //cout << c.str() << "\n";
    }
    bool sat;
    #if SOLVER==1
    sat = cdcl(clauses);
    #elif SOLVER==2
    sat = lookahead(clauses);
    #else
    sat = cubeAndConq(clauses);
    #endif

    //cout << "Result:" << sat << "\n";

    return 0;
}
