#include <queue>
#include <tuple>
#include <vector>

using namespace std;

#define Elem pair<unsigned int, double>

class Compare {
    public:
        bool operator()(Elem e0, Elem e1);
};

class VarGen {
    public:
        VarGen(unsigned int var_cnt, double activity_step);
        VarGen(unsigned int var_cnt, double activity_step, vector<double> activity);
        void update();
        void update(unsigned int var);
        void decay_all();
        void undo(unsigned int var);
        unsigned int get_var();
        const inline bool empty() {
            return vars.empty();
        }
        void reorder();
        void reset();
    private:
        double activity_step;
        vector<double> activity;
        vector<bool> stored;
        priority_queue<Elem, vector<Elem>, Compare> vars;
};
